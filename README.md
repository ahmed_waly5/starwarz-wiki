# README #

This README would help you to clone and setup this web service on your local machine.

### What is this repository for? ###

* This service is implemented as part of an evaluation for Software developer role at Redspace. 
* This service acts as a single endpiont that perform some data transformation based on several requests to the Star Wars API ( https://swapi.co ).
* Version 1.0

### How do I get set up? ###

* Make sure you have Node.js installed on your local machine
* Then clone this repo using this command on your command line git clone https://bitbucket.org/ahmed_waly5/starwarz-wiki/
* After it's done open the project folder using this command cd starwarz-wiki 
* Then run this command to install all the dependencies on you machine npm install --save
* After everything is installed start the service using this command npm start
* Service would start serving on localhost:80
* Service accept only one Get request on http://localhost/starwarz/api/people/:id
* The id is a number and it's the only variable in this request, it's the id that used to search for the characters on the Star Wars API ( https://swapi.co ). 


### Who do I talk to? ###

* Ahmed Waly
* ahmed.waly5@outlook.com