const express=require('express');
const app=express();
const axios = require('axios');



 app.get('/starwarz/api/people/:id',function(req,res){
  res.header("Access-Control-Allow-Origin", "http://localhost:3000"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  var personData='';
  var homePlanetData='';
  var filmsData=[]
  var filmArray=[];
  var filmDataArray=[];
  var id = req.params.id; 

  var url = 'https://swapi.co/api/people/'+id+'/?format=json';

  function getPersonData() {
    return new Promise(resolve => {
      axios.get('https://swapi.co/api/people/'+id+'/?format=json')
    .then(function (response) {
        personData=response.data;
        console.log('First request is done')
        resolve(personData);
      })
      .catch(function (error) {
        console.log(error);
      })
    });
  }

  function getPersonHomePlanet() {
    return new Promise(resolve => {
      axios.get(personData.homeworld)
    .then(function (response) {
        homePlanetData=response.data;
        console.log('Seconed request is done')
        resolve(homePlanetData);
      })
      .catch(function (error) {
        console.log(error);
      })
    });
  }
  function getPersonFilmsData() {
    return new Promise(resolve => {

      for(var i=0;i<personData.films.length;i++){
        filmArray.push(axios.get(personData.films[i]+'?format=json'));
      }
     axios.all(filmArray)
        .then(function (response) {
        for(var i=0;i<response.length;i++){
          filmDataArray.push(response[i].data);
        }
        filmsData=filmDataArray;
        console.log('Third request is done')
        resolve(filmsData);
      })
      .catch(function (error) {
        console.log(error);
      });
    });
  }

  async function asyncCall() {
    
    let result = []
    console.log('Performing first request')
    result.push({'person_data':await getPersonData()});
    console.log('Performing seconed request')
    result.push({'home_planet_data':await getPersonHomePlanet()});
    console.log('Performing third request')
    result.push({'films_data':await getPersonFilmsData()});
    console.log('Sending results')
    res.send(result);
  }
  
  asyncCall();
});

app.listen(80,function(){
   console.log('Server started on port 80...') 
});
